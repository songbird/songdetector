function [det_bins, net_output, threshold] = net_apply_for_song_count(SPEC, net, varargin)
%NET_APPLY_FOR_SONG_COUNT applies the two-layer perceptron net to detect a
%unique time point in a spectrogram of a stereotyped song
%   DET_BINS = NET_APPLY_FOR_SONG_COUNT(SPEC, NET)
%   returns the bin numbers DET_BINS of the detection point of the unique 
%   point the network NET is trained to detect. SPEC must be a (1xN 
%   dimensional) cell array of spectrograms. Each spectrogram SPEC{n} is a 
%   matrix of size SxT where the number of frequency bins S has to be the 
%   same for all spectrograms and the number of bins T can be different. 
%   DET_BINS is a 1xN dimensional cell array and specifies the bin IDs 
%   where the network output NET_OUTPUT goes above a manually set threshold.
%   The network NET must be trained with NET_APPLY_FOR_SONG_COUNT.
%
%   [DET_BINS, NET_OUTPUT] = NET_APPLY_FOR_SONG_COUNT(SPEC, NET) returns
%   the network output NET_OUTPUT of network NET for each spectrogram SPEC.
%
%   [DET_BINS, NET_OUTPUT, THRESHOLD] = NET_APPLY_FOR_SONG_COUNT(SPEC, NET) 
%   returns the THRESHOLD used on the NET_OUTPUT to get the detections of
%   the unique points in SPEC saved in POS_BINS.
%
%   [DET_BINS, NET_OUTPUT] = NET_APPLY_FOR_SONG_COUNT(..., 'PARAM1', val1, 'PARAM2', val2)
%   specifies optional parameter name/value pairs. If the parameters are 
%   not provided default values are used or the uer is asked to specify the
%   parameters as input. 
%   The parameters are: 
%
%   'threshold'             -   threshold on the network output to get the 
%                               detection points det_bins in SPEC.                                                      
%
% Example:
%       load('SPEC-b14g16-day1.mat')
%       load('noise')
%       [net, param] = net_train_for_song_count(SPEC, pos_bins,'win',[0 24],'noise',noise);
%       det_bins = net_apply_for_song_count(SPEC, net);
%       % Number of target syllalbles:
%       N = length(cell2mat(det_bins))
%
% ---
% Copyright: (c) 2020 ETH Zurich, Anja T. Zai, Richard H.R. Hahnloser 
%
% Reference (please cite):
% Undirected singing rate as a non-invasive tool for welfare monitoring in 
% isolated male zebra finches
% Homare Yamahachi, Anja T. Zai, Ryosuke O. Tachibana, Anna E. Stepien, 
% Diana I. Rodrigues, Sophie Cave-Lopez, Corinna Lorenz, Ezequiel Arneodo, 
% Nicolas Giret, Richard H. R. Hahnloser
% DOI: 10.1371/journal.pone.0236333
%
% Work address: Institute of Neuroinformatics, Winterthurerstrasse 8057,
% ETH Zurich and University of Zurich
% Website: https://gitlab.ethz.ch/songbird/songdetector
%
% Author: Richard H.R. Hahnloser and colleagues             Revision 0.0.0
% Author: Anja T. Zai            Date: 18-June-2020         Revision 1.0.0
%

% MAT-files required for testing the network:
%   net_apply.m


%------------- BEGIN CODE --------------

% check input:
if nargin<2
    error('Please specify SPEC and net.')
end
if ~iscell(SPEC) || isempty(SPEC)
    error('Input SPEC is not a cell array.')
end
if ~isnumeric(SPEC{1}) || ~isreal(SPEC{1}) || min(size(SPEC{1}))<2
    error('Input SPEC is not a cell array of matrices of real numbers.')
end
if length(unique(cellfun(@(x) size(x,1),SPEC,'UniformOutput',true)))>1
    error('Spectrograms provided in SPEC have to be matrices of size SxT where S has to be the same for all spectrograms.')
end

% Parse arguments and check if parameter/value pairs are valid
paramNames = {'threshold'};
defaults   = {[]};

[threshold] = internal.stats.parseArgs(paramNames, defaults, varargin{:});

% define: 
N = length(SPEC);
nf = size(SPEC{1},1);
wb = numel(net.meanX)/size(SPEC{1},1);

% Set threshold if not predefined
if isempty(threshold) || ~isnumeric(threshold) || ~isreal(threshold)
    threshold=[];
    figID = figure(10);clf;
    
    fprintf('Specify network threshold for detection (between -1 and 1): \n');
    while isempty(threshold) 
        net_plot_for_song_count(SPEC, [], [], net, [], figID);
        if isempty(threshold)
            threshold=input(' ');
        end
    end
    if ~isnumeric(threshold) || ~isreal(threshold) || threshold > 1 || threshold < -1
        fprintf('Threshold set incorrectly -> set to default = 0.5 \n')
        threshold = 0.5;
    end
   net_plot_for_song_count(SPEC, [], [], net, threshold, figID);
   pause(0.001)
end

%% 

net_output = cell(1,N);
det_bins = cell(1,N);

parfor n=1:N
    
    % calculate network output for spectrogram SPEC{n}
    net_output{n} = net_apply(SPEC{n}, net, nf, wb);
    
    % get detection points where net_output goes above threshold
    above_thresh = find(net_output{n} > threshold);
    if ~isempty(above_thresh)
        det_bins{n} = above_thresh([1 find(diff(above_thresh)>1)+1]);
    else
        det_bins{n} = []; 
    end
    
end


end

