function net_output = net_apply(spec, net, nf, wb)
%NET_APPLY applies the two-layer perceptron net to a single spectrogram.
%   NET_OUTPUT = NET_APPLY(SPEC, NET) returns the two-layer neural network
%   output for a single spectrogram SPEC (SxT matrix) and a network NET trained
%   by net_train_for_song_count.m where S is the number of frequency bins
%   and T the number of time bins. Output NET_OUTPUT has size 1xT. 
%
%   This function is called by:     net_apply_for_song_count.m
%                                   net_plot_for_song_count.m 
%

% ---
% Copyright: (c) 2020 ETH Zurich, Anja T. Zai, Richard H.R. Hahnloser 
%
% Reference (please cite):
% Undirected singing rate as a non-invasive tool for welfare monitoring in 
% isolated male zebra finches
% Homare Yamahachi, Anja T. Zai, Ryosuke O. Tachibana, Anna E. Stepien, 
% Diana I. Rodrigues, Sophie Cave-Lopez, Corinna Lorenz, Ezequiel Arneodo, 
% Nicolas Giret, Richard H. R. Hahnloser
% DOI: 10.1371/journal.pone.0236333
%
% Work address: Institute of Neuroinformatics, Winterthurerstrasse 8057,
% ETH Zurich and University of Zurich
% Website: https://gitlab.ethz.ch/songbird/songdetector
%
% Author: Richard H.R. Hahnloser and colleagues             Revision 0.0.0
% Author: Anja T. Zai            Date: 18-June-2020         Revision 1.0.0
%


%------------- BEGIN CODE --------------

if nargin<3
    nf = size(spec,1);
end

if nargin<4
    wb = numel(net.meanX)/nf;
end

net_output = -1*ones(1, size(spec,2));
if length(net_output) > wb
    
    y0 = reshape(spec,numel(spec),1);
    
    for i = 1 : length(net_output)-wb+1
        
        y1 = y0((i-1)*nf+1:(i-1)*nf+nf*wb);
        perc_value = TestRecognizer(y1, net.w1,  net.w2,  net.b1,  net.b2);
        
        net_output(i+wb-1) = perc_value;
    end
    
end
end

%----------------Subfunctions--------------------------------------------

function Yt2 = TestRecognizer(Xt, w1, w2, b1, b2)

    size_Xt = size(Xt, 2);
    Yt2 = zeros(1, size_Xt);

    if w2 == 0 & b2 == 0 %#ok<AND2> because w2 and b2 are matrices
        
        % you probably want to change this code to the version that is used
        % below ...
        for i = 1 : size_Xt
            Yt2(i) = w1 * Xt(:,i) + b1;
        end
        
    else
        
        %% old version - super slow!
        %for i = 1 : size_Xt
        %    Ytth = tansig(w1 * Xt(:,i) + b1);
        %    Yt2(i) = w2 * Ytth + b2;
        %end
        %% vectorized version - about 200 times faster.
        
        Ytth = tansig(w1 * Xt(:, 1 : size_Xt) + repmat(b1, 1, size_Xt));
        Yt2 = w2 * Ytth + b2;
        
    end

end