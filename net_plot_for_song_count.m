function [figID] = net_plot_for_song_count(SPEC, pos_bins, det_bins, net, threshold, figID)
%NET_PLOT_FOR_SONG_COUNT plots the network response and/or trainging data
%together with an example spectrogram
%   figID = NET_PLOT_FOR_SONG_COUNT(SPEC, POS_BINS) plots a random example
%   spectrogram from cell array SPEC (dimesion 1xN) and marks the 
%   spectrogram bins of the training data provided by POS_BINS for the 
%   given example spectrogram. If N=1 only this spectrogram is displayed.
%   If N>1 a button is created above the spectrogram to switch between
%   random examples. 
% 
%   figID = NET_PLOT_FOR_SONG_COUNT(SPEC, [], DET_BINS) plots a random 
%   example spectrogram and marks the spectrogram bins of the detection  
%   points provided in DET_BINS in red where DET_BINS is a cell array with
%   dimention 1xN. 
%
%   figID = NET_PLOT_FOR_SONG_COUNT(SPEC, [], [], NET) plots a random
%   example spectrogram from SPEC and the network level defined by the 
%   parameters in net.
%
%   figID = NET_PLOT_FOR_SONG_COUNT(SPEC, [], [], NET, threshold) 
%   additionally plots the THRESHOLD.

%   figID = NET_PLOT_FOR_SONG_COUNT(SPEC, [], [], NET, [],figID) uses 
%   Figure figID to plot.
%
%   figID = NET_PLOT_FOR_SONG_COUNT(SPEC, POS_BIN, DET_BIN, NET, thresh, figID) 
%   plots all the above together.                                                   
%
% Example:
%       load('SPEC-b14g16-day1.mat')
%       load('noise')
%       net_plot_for_song_count(SPEC,pos_bins);
%       [net, param] = net_train_for_song_count(SPEC, pos_bins,'win',[0 24],'noise',noise);
%       net_plot_for_song_count(SPEC,pos_bins,[],net,0.5);
%       load('SPEC-b14g16-day2.mat')
%       det_bins = net_apply_for_song_count(SPEC, net);
%       net_plot_for_song_count(SPEC,pos_bins,det_bins,net,0.5);
%
% ---
% Copyright: (c) 2020 ETH Zurich, Anja T. Zai, Richard H.R. Hahnloser 
%
% Reference (please cite):
% Undirected singing rate as a non-invasive tool for welfare monitoring in 
% isolated male zebra finches
% Homare Yamahachi, Anja T. Zai, Ryosuke O. Tachibana, Anna E. Stepien, 
% Diana I. Rodrigues, Sophie Cave-Lopez, Corinna Lorenz, Ezequiel Arneodo, 
% Nicolas Giret, Richard H. R. Hahnloser
% DOI: 10.1371/journal.pone.0236333
%
% Work address: Institute of Neuroinformatics, Winterthurerstrasse 8057,
% ETH Zurich and University of Zurich
% Website: https://gitlab.ethz.ch/songbird/songdetector
%
% Author: Anja T. Zai            Date: 18-June-2020         Revision 1.0.0
%

% MAT-files required for training and testing network:
%   net_apply.m


%------------- BEGIN CODE --------------

% check input
if nargin < 1 || ~iscell(SPEC) || ~isnumeric(SPEC{1}) || ~isreal(SPEC{1})
   error('Spectrogram input SPEC has to be a cell array of two dimensional matrices.') 
end

if nargin < 2
    pos_bins = [];
else
    if ~isempty(pos_bins) && (~iscell(pos_bins) || ~isnumeric(cell2mat(pos_bins)) || ~isreal(cell2mat(pos_bins)))
        error('Input pos_bins has to be a cell array of vectors (or empty).') 
    end
end

if nargin < 3
    det_bins = [];
else
    if ~isempty(det_bins) && (~iscell(det_bins) || ~isnumeric(cell2mat(det_bins)) || ~isreal(cell2mat(det_bins)))
        error('Input det_bins has to be a cell array of vectors (or empty).') 
    end
end

if nargin < 4
    net = [];
end
if nargin < 5
    threshold = [];
end

% create figure
if nargin < 6 || isempty(figID)
    figID = figure;
else
    figure(figID);clf
end
% create axis
if nargin > 3 && ~isempty(net)
    ax1 = subplot(2,1,1);
    ax2 = subplot(2,1,2);
    linkaxes([ax1,ax2],'x');
else
    ax1 = subplot(1,1,1);
    ax2 = [];
end

% plot 
replot_for_song_count([], [], ax1, ax2, SPEC, pos_bins, det_bins, net, threshold)

% if more than one spectrogram is provided, create a button to switch
% between them (in random order)
if length(SPEC)>1
    uicontrol('Parent', figID, 'Style', 'pushbutton',...
        'String', 'Switch displayed example',...
        'Visible','on', 'units','normal',...
        'position',[0.36 0.93 0.3 0.05],...
        'callback', {@replot_for_song_count, ...
        ax1, ax2, SPEC, pos_bins, det_bins, net, threshold});
end

end

%----------------Subfunctions--------------------------------------------

function replot_for_song_count(src, evt, ax1, ax2, SPEC, pos_bins, det_bins, net, threshold)

% randomly choose spectrogram to display
N=length(SPEC);
ri=randi(N);

% plot spectrogram
axes(ax1),hold off
imagesc(SPEC{ri}),hold on
axis xy,ylabel('Frequency (bin)')
text(24, 136, sprintf('SPEC #%d', ri));
p1=[];p2=[];

% plot positive examples:
if ~isempty(pos_bins) && ~isempty(pos_bins{ri})
    
    if size(pos_bins{ri},1)>size(pos_bins{ri},2)
        pos_bins{ri}=pos_bins{ri}';
    end
    
    n_pb = max(size(pos_bins{ri}));
    x = repmat(pos_bins{ri}, 2, 1);
    y = [0.5*ones(1, n_pb); ...
        size(SPEC{ri},1) / 2*ones(1, n_pb)];
    
    p1=plot(x,y,'w','LineWidth',2);
    
end

% plot detection points
if ~isempty(det_bins)
    
    if size(det_bins{ri},1)>size(det_bins{ri},2)
        det_bins{ri}=det_bins{ri}';
    end
    
    n_db = max(size(det_bins{ri}));
        
    x = repmat(det_bins{ri}, 2, 1);
    y = [0.5*ones(1, n_db); ...
        size(SPEC{ri},1) / 2*ones(1, n_db)];

    p2=plot(x, y,'r','LineWidth',2);
    
end
if isempty(p2) && ~isempty(p1)
    Leg=legend(p1(1),'Training data');
elseif ~isempty(p2) && isempty(p1)
    Leg=legend(p2(1),'Network output (detection points)');
elseif ~isempty(p2) && ~isempty(p1)
    Leg=legend([p1(1),p2(1)],{'Training data','Network output (detection points)'});
end

if exist('Leg','var')
    legend boxoff
    set(Leg,'TextColor',[1 1 1]) 
end

if isempty(net)
    xlabel('Time (bin)')
else
    axes(ax2),hold off
    netoutput = net_apply(SPEC{ri}, net);
    plot(netoutput)
    axis tight,ylim([-1.02 1])
    xlabel('Time (bin)'),ylabel('Network level')
    
    if ~isempty(threshold)
        hold on,
        plot([0.5 length(netoutput)+0.5],threshold*[1 1],'r')
        legend({'Network level','Threshold'})
    end
end

end

