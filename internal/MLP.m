% [W1, B1, W2, B2] = ...
% MLP(patterns, target, num_hidden, max_iterations, batch_size, replace_count)
%
% implementation of a two layer perceptron to discriminate two classes of
% patterns, i.e. the target is binary, 0 or 1.
%
% patterns: NxK matrix, where each row n shows a pattern of K elements
% target: logical vector of size N
% num_hidden: number of hidden units. default value is 4
% max_iterations: maximum number of training iterations
% batch_size: size of batch training set
% replace_count: number of patterns that are replaced per iterations
%
% the perceptron will be trained via a batch method and a RPROP
% implementation. the training will calculate the derivatives of the
% weights based on a group of patterns (batch_size) and exchange some of
% the patterns from time to time (replace_count). the RPROP method will not
% speed up, if all the pattern will change in each operation.


function [W1, B1, W2, B2] = MLP(patterns, target, num_hidden, max_iterations, batch_size, replace_count)

%% check the inputs and try to correct...
if nargin < 2
    disp('not enough input parameters')
    return
end

if nargin < 3
    num_hidden = 4;
end

if nargin < 4
    max_iterations = 2500;
end

if nargin < 5
    batch_size = 20;
end

if nargin < 6
    replace_count = 5;
end

if ~isvector(target)
    return
end

if size(target,2) > size(target,1)
    target = target';
end

% get number of patterns to train on
number_of_patterns = size(target,1);
if size(patterns,2) ~= number_of_patterns
    if size(patterns,2) == number_of_patterns
        patterns = patterns';
        disp('warning: training patterns are not in correct format')
    else
        disp('patterns and target dimension do not match')
        return
    end
end
% get length of pattern
length_of_patterns = size(patterns,1);

%% specify how to train, how many patterns per batch, how many to replace
batch_size = min(batch_size, number_of_patterns);
number_patterns_replace = min(replace_count, number_of_patterns);

% choose first batch to train
indices = 1:number_of_patterns;
[~,p] = sort(rand(1,number_of_patterns));
indices_used = p(1:batch_size);

%% init parameters
% initial weights and biasesuniform on [-1,1]
scale=0.1;
W1 = scale*(2*rand(num_hidden, length_of_patterns)-1);
B1 = scale*(2*rand(num_hidden,1)-1);
W2 = scale*(2*rand(1,num_hidden)-1);
B2 = scale*(2*rand-1);

% initial rprep step width
gamma_W1 = 0*W1+0.01;
gamma_W2 = 0*W2+0.01;
gamma_B1 = 0*B1+0.01;
gamma_B2 = 0*B2+0.01;

% initial gradients
dW1 = 0*W1;
dW2 = 0*W2;
dB1 = 0*B1;
dB2 = 0*B2;

% train on 'number_of_patterns' simultaneously
dW1_train = zeros(num_hidden, length_of_patterns, batch_size);
dB1_train = zeros(num_hidden, batch_size);
dW2_train = zeros(num_hidden, batch_size);
dB2_train = zeros(batch_size,1);

%% train the perceptron

maxW1 = zeros (1,max_iterations);
meanW1 = zeros (1,max_iterations);
medianW1 = zeros (1,max_iterations);

tic
for iteration = 1 : max_iterations 
    
    if all(all(gamma_W1 < 1e-5))
        disp(['stopping criterion reached - iteration' num2str(iteration)])
        break
    end
    
    if iteration/100 == floor(iteration/100)
        disp(['iteration ' num2str(iteration)])
    end
    % choose new (indices_used) patterns to train on
    used = randperm(batch_size);
    indices_keep = indices_used(used(1:end-number_patterns_replace));
    indices_not_used = indices;
    indices_not_used(indices_keep) = [];
    not_used = randperm(number_of_patterns - batch_size);
    indices_used = [indices_keep indices_not_used(not_used(1:number_patterns_replace))];
    
    % update the old gradients (t-1) -> (t)
    dW1_old = dW1;
    dB1_old = dB1;
    dW2_old = dW2;
    dB2_old = dB2;
    
    % compute new gradients (t) on several (batch_size) patterns,
         
    % slice the variables for parallel execution
    patterns_tmp = patterns(:,indices_used);
    target_tmp = target(indices_used);
    
    parfor count = 1:batch_size
        [dW1_train(:,:,count), dB1_train(:,count), dW2_train(:,count), dB2_train(count)] = ...
            derivativeMLP(patterns_tmp(:,count), target_tmp(count), W1, B1, W2, B2);
    end
    
    % derivative of sum is the sum of derivative...
    dW1 = sum(dW1_train,3);
    dB1 = sum(dB1_train,2);
    dW2 = sum(dW2_train,2)';
    dB2 = sum(dB2_train,1);
    
    % update the weights via resilient propagation
    [W1, gamma_W1] = rprop (W1, gamma_W1, dW1, dW1_old);
    [W2, gamma_W2] = rprop (W2, gamma_W2, dW2, dW2_old);
    [B1, gamma_B1] = rprop (B1, gamma_B1, dB1, dB1_old);
    [B2, gamma_B2] = rprop (B2, gamma_B2, dB2, dB2_old);
    
    maxW1(iteration) = max(max(gamma_W1));
    meanW1(iteration) = mean(mean(gamma_W1));
    medianW1(iteration) = median(median(gamma_W1));
end
toc
figure(98);clf;semilogy(maxW1);hold on;semilogy(medianW1,'r');semilogy(meanW1,'k')
legend('max weight change','median weight change','mean weight change','Location','NorthEast')
xlabel('Training iterations'),ylabel('Weight change')
title('Figure to evaluate training success: weight change')

end

