%% calculate MLP output

% Z = W2 * tansig(W1 * X + B1) + B2 two layer perceptron
% C = 1/2 * (Z - T)^2 cost function, output - target squared
% d/dB2 C = (Z - T)
% d/dW2 C = (Z - T) * tansig(W1 * X + B1)
% d/dB1 C = (Z - T) * W2 * tansig'(W1 * X + B1)
% d/dW1 C = (Z - T) * W2 * tansig'(W1 * X + B1) X

function [MLP_output, HL_raw, HL_tansig, HL_d_tansig] = evalMLP (X, W1, B1, W2, B2)
% multi-layer-perceptron-output and hidden-layer-values
% using the tansig (tanh) transfer function
% tansig(x) = tanh(x) = 2/(1+exp(-2x))-1
% d/dx tanh(x) = sech(x)^2 = (1/cosh(x))^2 = (2/(exp(x)+exp(-x)))^2

HL_raw = W1 * X + B1;
%HL_tansig = tansig(HL_raw);
HL_tansig=(2./(1+exp(-2*HL_raw))-1);
MLP_output = W2 * HL_tansig + B2;
if nargout == 4
    HL_d_tansig = (2./(exp(HL_raw)+exp(-HL_raw))).^2;
end

end