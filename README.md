# SongDetector

Code for training and applying a two-layer neural network to detect a unique point in spectrograms of a steretyped bird's song. 

See net_EXAMPLE.m for an example application and instructions of how to use the code.

Required toolbox: 
Deep Learning Toolbox
Parallel Computing Toolbox

Main functions used for training, applying and visualizing network:
net_train_for_song_count.m      % train network
net_apply_for_song_count.m      % apply network and get detection points
net_plot_for_song_count.m       % visualize data and network

Required additional MAT-files:
MLP.m
evalMLP.m
derivativeMLP.m
net_apply.m
rprop.m

This code is licensed via the MIT License (see included file LICENSE).
Copyright: (c) 2020 ETH Zurich, Anja T. Zai, Richard H.R. Hahnloser 

Reference (please cite):
Undirected singing rate as a non-invasive tool for welfare monitoring in isolated male zebra finches
Homare Yamahachi, Anja T. Zai, Ryosuke O. Tachibana, Anna E. Stepien, Diana I. Rodrigues, Sophie Cave-Lopez, Corinna Lorenz, Ezequiel Arneodo, Nicolas Giret, Richard H. R. Hahnloser
DOI: 10.1371/journal.pone.0236333 	

Email: rich@ini.ethz.ch
Work address: Institute of Neuroinformatics, Winterthurerstrasse 8057,
ETH Zurich and University of Zurich
Website: https://gitlab.ethz.ch/songbird/songdetector

