function [net, param] = net_train_for_song_count(SPEC, pos_bins, varargin)
%NET_TRAIN_FOR_SONG_COUNT trains a two-layer neural network to detect a
%unique time point in a spectrogram of a stereotyped song
%   NET = NET_TRAIN_FOR_SONG_COUNT(SPEC, POS_BINS)
%   returns the network parameters of a two-layer neural network trained to 
%   detect the training data POS_BINS in the spectrograms SPEC, where both 
%   POS_BINS and SPEC must be cell arrays. SPEC is a 1xN dimensional cell  
%   array (N = number of individual spectrograms) where each entry contains 
%   one spectrogram of size SxT. The number of frequency bins S has to be  
%   the same for all spectrograms SPEC{n} whereas the duration T of each 
%   spectrogram can vary. POS_BINS is a 1xN dimensional cell array of 
%   training data and specifies the bin IDs of a unique point in the song 
%   (e.g. target syllable). POS_BINS{n} has the length of the number of  
%   occurences of this unique point in the spectrogram SPEC{n}. Thus if the 
%   unique point occurs in the 100th and the 250th bin in SPEC{n} then 
%   POS_BINS{n} = [100 250], if there are no occurances then 
%   POS_BINS{n} = []; Default values are tuned for zebra finch songs and  
%   spectrogram buffersize 4 ms.
%
%   [NET, PARAM] = NET_TRAIN_FOR_SONG_COUNT(SPEC, POS_BINS) returns
%   the parameters PARAM used for training the network
%
%   [NET, PARAM] = NET_TRAIN_FOR_SONG_COUNT(..., 'PARAM1', val1, 'PARAM2', val2)
%   specifies optional parameter name/value pairs. If the parameters are 
%   not provided, default values are used or the uer is asked to specify 
%   the parameters as input. 
%   The parameters are: 
%
%   'win'                   -   Window [start end] used for detecting the 
%                               training data POS_BINS relative to the bins  
%                               specified in POS_BINS.
%                               Examples 1: win = [0 24] will use 25 bins
%                               starting at the bins specified in POS_BINS
%                               and thus the detection point in the test 
%                               data will be 24 bins after the unique
%                               points specified in POS_BINS. 
%                               Examples 2: win = [-24 0] will use 25 bins
%                               ending at the bins specified in POS_BINS
%                               and thus the detection point in test data
%                               will be at the unique point specified.                                                        
%
%   'noise'                 -   Example spectrogram of cage noise when no
%                               vocalization is present. This spectrogram 
%                               is added to the negative examples collected 
%                               from the spectrograms SPEC to avoid 
%                               detection of cage noise. 
%
%   'neg_example_factor'    -   Factor defining the amount of training
%                               data used as negative examples relative to 
%                               the positive examples provided in POS_BINS.
%                               For speed and performance it is adviced to 
%                               reduce the negative examples to e.g. a
%                               factor 30.
%   
%   'num_ignore'            -   Number of buffers ignored pre/post POS_BINS
%                               (Default = 6).
%
%   'offset'                -   Number of buffers ignored at the end of
%                               each spectrogram SPEC{n} (default = 200).
%
%   'spec_thresh'           -   Threshold of summed spectrogram power above 
%                               which spectrogram bins are taken as 
%                               negative examples. 
% 
%   'thresh_halfwidth'      -   Number of buffers taken as negative 
%                               examples  before/after threshold crossing.
%
%   'num_pcs'               -   Number of principle components taken to
%                               train the network.
%
%   'num_hidden'            -   Number of units in hidden layer
%
%   'max_iterations'        -   Maximum number of training iterations
%
%
% Example:
%       load('SPEC-b14g16-day1.mat')
%       load('noise')
%       [net, param] = net_train_for_song_count(SPEC, pos_bins,'win',[0 24],'noise',noise);
%       det_bins = net_apply_for_song_count(SPEC, net);
%       % Number of target syllalbles:
%       N = length(cell2mat(pos_bins))
%       
% ---
% Copyright: (c) 2020 ETH Zurich, Anja T. Zai, Richard H.R. Hahnloser 
%
% Reference (please cite):
% Undirected singing rate as a non-invasive tool for welfare monitoring in 
% isolated male zebra finches
% Homare Yamahachi, Anja T. Zai, Ryosuke O. Tachibana, Anna E. Stepien, 
% Diana I. Rodrigues, Sophie Cave-Lopez, Corinna Lorenz, Ezequiel Arneodo, 
% Nicolas Giret, Richard H. R. Hahnloser*
% DOI: 10.1371/journal.pone.0236333
%
% * email: rich@ini.ethz.ch
% Work address: Institute of Neuroinformatics, Winterthurerstrasse 8057,
% ETH Zurich and University of Zurich
% Website: https://gitlab.ethz.ch/songbird/songdetector
%
% Author: Richard H.R. Hahnloser and colleagues             Revision 0.0.0
% Author: Anja T. Zai            Date: 18-June-2020         Revision 1.0.0
%

% MAT-files required for training the network:
%   MLP.m
%   evalMLP.m
%   rprop.m

%------------- BEGIN CODE --------------

% check input:
if nargin<2
    error('Please specify spectrogram SPEC and training data pos_bins.')
end
if ~iscell(SPEC) || ~iscell(pos_bins) || isempty(SPEC) || isempty(pos_bins)
    error('Input SPEC or pos_bins is not a cell array.')
end
[n11, n12] = size(SPEC);
[n21, n22] = size(pos_bins);
if max([n11 n12])~=max([n21 n22]) || min([n11 n12 n21 n22])~=1
    error('Input SPEC and pos_bin have to have the same length.')
end
if ~isnumeric(SPEC{1}) || ~isreal(SPEC{1}) || min(size(SPEC{1}))<2
    error('Input SPEC is not a cell array of matrices of real numbers.')
end
if length(unique(cellfun(@(x) size(x,1),SPEC,'UniformOutput',true)))>1
    error('Spectrograms provided in SPEC have to be matrices of size SxT where S has to be the same for all spectrograms.')
end
if ~isnumeric(cell2mat(pos_bins)) || ~isreal(cell2mat(pos_bins)) || any(floor(cell2mat(pos_bins))~=cell2mat(pos_bins)) || any(cell2mat(pos_bins)<1)
    errro('Input pos_bins{n} have to be positive integers for all n (specifying bins within SPEC{n}).')
end
if any(cellfun(@(x,y) any(y>size(x,2)),SPEC,pos_bins,'UniformOutput',true))
    error('pos_bins{n} has to specify bins inside SPEC{n}. Thus if SPEC{n} has size SxT, no pos_bins{n} can be larger than T.')
end

% Parse arguments and check if parameter/value pairs are valid
paramNames = {'win', 'noise', 'neg_example_factor', 'num_ignore', 'offset','spec_thresh', 'thresh_halfwidth', 'num_pcs', 'num_hidden','max_iterations'};
defaults   = {[],[],-1,6,200,[],13,20,3,300};

[win, noise, neg_example_factor, num_ignore, offset, spec_thresh,thresh_halfwidth, num_pcs, num_hidden, max_iterations]...
        = internal.stats.parseArgs(paramNames, defaults, varargin{:});
    
% win
if isempty(win) || length(win)>2 || diff(win)<0 || ~isnumeric(win) || ~isreal(win)
     fprintf('Window used for detection is not provided as an input. \n');
     fprintf('Negative window sizes result in detection points at the bins \n');
     fprintf('of the training data and positive window sizes result in \n');
     fprintf('detection points after the bins of the training data. \n');
     fprintf('Specify window size in spectrogram bins (default = -25): \n');
     win = input(' ');
end
if length(win)<2
    if isempty(win)
        win = [-24 0];
    elseif win > 0
        win = [0 win-1];
    else
        win = [-win+1 0];
    end
end
if length(win)~=2
    fprintf('ERROR: Invalid window. Specify window in spectrogram bins [before after] training data (e.g. [0 24]) \n');
    return
end

fprintf('\n####### 2-layer neural network training starts ####### \n\n')


% neg_example_factor
if isempty(neg_example_factor) || ~isnumeric(neg_example_factor) || ~isreal(neg_example_factor) || length(neg_example_factor)>1 || neg_example_factor<0
    fprintf('There are %d positive examples provided by the user. \n\n',length(cell2mat(pos_bins)));
    fprintf('There are %.0f times more negative examples in the data set. \n\n', size(cell2mat(SPEC),2)/length(cell2mat(pos_bins)));
    fprintf('To improve on training time and on performance it is adviced to \n')
    fprintf('reduce the number of negative examples (e.g. to a factor 30). \n');
    fprintf('Reduce negative examples to factor (enter = all): ');
    neg_example_factor=input(' ');
    fprintf('\n');
    if isempty(neg_example_factor)
        neg_example_factor=floor(size(cell2mat(SPEC),2)/length(cell2mat(pos_bins)));
    end
end
neg_example_factor=min(neg_example_factor,floor(size(cell2mat(SPEC),2)/length(cell2mat(pos_bins))));

% noise
if ~isnumeric(noise) || ~isreal(noise) || (~isempty(noise) && size(noise,1)~=size(SPEC{1},1))
    error('Noise has to be a spectrogram with the same number of frequency bins as SPEC')
end

% num_ignore
if ~isnumeric(num_ignore) || ~isreal(num_ignore) || floor(num_ignore)~=num_ignore || num_ignore<0
    error('num_ignore has to be a positive integer')
end

% offset
if ~isnumeric(offset) || ~isreal(offset) || floor(offset)~=offset || offset<0
    error('offset has to be a positive integer')
end

% perceptron_spec_thresh
if ~isnumeric(spec_thresh) || ~isreal(spec_thresh) || length(spec_thresh)>1
    error('spec_thresh has to be a single number (or empty -> to be defined later)')
end

% thresh_halfwidth
if ~isnumeric(thresh_halfwidth) || ~isreal(thresh_halfwidth) || floor(thresh_halfwidth)~=thresh_halfwidth || thresh_halfwidth<0
    error('thresh_halfwidth has to be a positive integer')
end

% perceptron_num_pcs
if ~isnumeric(num_pcs) || ~isreal(num_pcs) || floor(num_pcs)~=num_pcs || num_pcs<0
    error('num_pcs has to be a positive integer')
end

% perceptron_num_hidden
if ~isnumeric(num_hidden) || ~isreal(num_hidden) || floor(num_hidden)~=num_hidden || num_hidden<0
    error('num_hidden has to be a positive integer')
end

% max_iterations
if ~isnumeric(max_iterations) || ~isreal(max_iterations) || floor(max_iterations)~=max_iterations || max_iterations<0
    error('max_iterations has to be a positive integer')
end


%% Convert input

pos_exampl_specID = cell2mat(cellfun(@(x,y) x*ones(size(y)),...
    num2cell(1:length(pos_bins)),pos_bins,'UniformOutput',false));
pos_exampl_binsM = cell2mat(pos_bins);

N = length(SPEC);
wl = (diff(win)+1)*size(SPEC{1},1);
pos_exampl_specIDU = unique(pos_exampl_specID);


%% take_out: take out surrounding of 'pos_exampl_specID' and end of file (defined by offset)
take_out=cell(1,N);
for ii=1:length(pos_exampl_specIDU)
    
    h=zeros(1,size(SPEC{pos_exampl_specIDU(ii)},2));
    on=pos_exampl_binsM(pos_exampl_specID==pos_exampl_specIDU(ii)) + win(1);

    for j=-num_ignore:num_ignore
        on2=on(on+j<size(h,2)+1 & on+j>0);
        h(on2+j)=1;
    end
    h(end-offset:end)=1;
    take_out{pos_exampl_specIDU(ii)}=h;
end


%% leave_in
leave_in=cell(1,N);
for ii=1:N
    
    h_in=zeros(1,size(SPEC{ii},2));
    i0=pos_exampl_binsM(pos_exampl_specID==ii);
    on=i0+win(1); %+1
    on=on(on>0 & on<length(h_in)+2); % why +2?
    h_in(on)=1;
    leave_in{ii}=h_in;
    
end

%% define positive training data: X_pos 
X_pos=zeros(wl,length(pos_exampl_binsM));
cin=0;

for ii=1:N
    h=SPEC{ii};
    lv_in=find(leave_in{ii});
    for j=lv_in
        cin=cin+1;
        X_pos(:,cin)=reshape(h(:,j:j+diff(win)),wl,1);
    end
end

%% get threshold
fig9 = figure;
ax2=subplot(3,1,2:3);ax1=subplot(3,1,1);
linkaxes([ax1,ax2],'x');
fig9_KeyPressFcn([],[], SPEC,ax1,ax2)
uicontrol('Parent',fig9,'Style','pushbutton','String','Switch displayed example',...
    'Visible','on','units','normal','position',[0.36 0.93 0.3 0.05],'callback',@(src, evt) fig9_KeyPressFcn(src, evt, SPEC,ax1,ax2,spec_thresh,thresh_halfwidth));

if isempty(spec_thresh)
    uicontrol('Parent',fig9,'Style','pushbutton','String','Input threshold',...
    'Visible','on','units','normal','position',[0.75 0.55 0.15 0.05],'callback','h_g=gcbf;h_g.Name = num2str(1);');

    fprintf('The new figure displays the summed power over all frequency bins \n')
    fprintf('as a function of time for an example spectrogram. To display \n')
    fprintf('another example press the button above the spectrogram. \n')
    fprintf('In the figure, determine the threshold that distinguishes between song and silence. \n')
    fprintf('When you are ready to input the threshold press the button on the plot. \n')
    
    waitfor(fig9,'Name')
    
    spec_thresh=input('Input threshold: ');
    fprintf('\n');
end

if ~isnumeric(spec_thresh) || ~isreal(spec_thresh) || length(spec_thresh)>1
    error('spec_thresh has to be a single number (or empty -> to be defined later)')
end

% plot threshold
fig9_KeyPressFcn([],[], SPEC,ax1,ax2,spec_thresh,thresh_halfwidth)
pause(0.0001)

%% define negative training data: X_neg
X_neg=zeros(size(X_pos,1),neg_example_factor*size(X_pos,2)); % negative training data
step_out=0;
rp=randperm(N);
ci=0; 
for ii=1:length(pos_exampl_specIDU)
    h=double(SPEC{rp(ii)});
    hloud=sum(h,1)>spec_thresh;
    hloud=conv(double(hloud),ones(2*thresh_halfwidth,1)/2/thresh_halfwidth);
    hloud=hloud(thresh_halfwidth:end-thresh_halfwidth)>0;
    tk_out=find(take_out{rp(ii)});
    
    ok=find(~ismember(1:(size(h,2)-diff(win)),tk_out) & hloud(1:end-diff(win)));
    if  ci>size(X_neg,2)-length(ok) % stopping criterion
        ok=ok(1:min(length(ok),size(X_neg,2)-ci));
        step_out=1;
    end
    
    for j=ok
        ci=ci+1;
        X_neg(:,ci)=reshape(h(:,j:j+diff(win)),wl,1);
    end
    if step_out
        fprintf('%d negative examples from %d spectrograms taken, \n',ci,ii);
        fprintf('defined as bins with summed power above threshold plus \n');
        fprintf('%d bins before and after.\n\n',round(thresh_halfwidth));
        break
    end
end
X_neg=X_neg(:,1:ci);

%% add noise+calls
if nargin > 3 && ~isempty(noise)
    fprintf('Now adding background noise \n\n');
    Xnoise = zeros(size(X_pos,1),size(noise, 2)-diff(win)-1);
    for ii = 1:size(Xnoise, 2)
        Xnoise(:,ii) = reshape(noise(:,ii:ii+diff(win)),wl,1);
    end
else
    Xnoise = [];
end

%% Train two-layer neural network 

% define input
Y=[ones(1,size(X_pos,2)) -ones(1,size(X_neg,2)) -ones(1,size(Xnoise,2))];
Xp=[X_pos X_neg Xnoise];

% this is a hack because the recoorder plugin has no 'mean'  input
meanXp=0*mean(Xp,2);

cMat=Xp*Xp'/size(Xp,2);
[E,~] = eigs(cMat, num_pcs);
netPCs=E';

PCcoeffs = netPCs*Xp; % coefficients (compressed data)

% train perceptron on 0.5 the data and exchange 0.05 per
% iteration
[W1, B1, W2, B2] = MLP(PCcoeffs, Y, num_hidden, max_iterations, ceil(size(PCcoeffs,2)/2), ceil(size(PCcoeffs,2)/20));
MLP_output=0*Y;
for i=1:size(PCcoeffs,2)
    MLP_output(i) = evalMLP (PCcoeffs(:,i), W1, B1, W2, B2);
end
figure(99);clf; plot(Y,'r','LineWidth',1.5);hold on; plot(MLP_output,'k');
ylabel('Network output'),xlabel('Training data samples')
legend({'Target output','Network output'})
title('Figure to evaluate training success: network output')

% write output
net.w1 = W1*netPCs;
net.b1 = B1;
net.w2 = W2;
net.b2 = B2;
net.meanX=meanXp;
fprintf('Network trained.\n\n');
fprintf('Two figures are plotted to visually evaluate network training. \n')

% write parameters: 
param.win = win;
param.noise = noise;
param.neg_example_factor = neg_example_factor;
param.num_ignore = num_ignore;
param.offset = offset;  % ceil(scanrate/nonoverlap); % 1 second offset
param.thresh_halfwidth = thresh_halfwidth; % = 50/1000*32000/128; halfwidth = ceil(50/1000*scanrate/nonoverlap)
param.num_pcs = num_pcs;
param.hidden = num_hidden;
param.spec_thresh = spec_thresh;
param.max_iterations = max_iterations;


end


%----------------Subfunctions--------------------------------------------

function fig9_KeyPressFcn(src, evt, SPEC,ax1,ax2,spec_thresh,thresh_halfwidth)

axes(ax2),hold off
ri=randi(length(SPEC)); h=SPEC{ri}; s=sum(h,1);
plot(s); axis tight
xlabel('Time (bins)'), ylabel('Summed power')
axes(ax1);
imagesc(SPEC{ri}),axis xy
text(24, 140, sprintf('SPEC #%d', ri));
ylabel('Frequemcy bins')

if nargin>6 && ~isempty(spec_thresh) && ~isempty(thresh_halfwidth)
    axes(ax2);hold on;
    plot(get(gca,'xlim'),spec_thresh*ones(size(get(gca,'xlim'))),'r');
    hloud=sum(h,1)> spec_thresh;
    hloud=conv(double(hloud),ones(2*thresh_halfwidth,1));
    hloud=hloud(thresh_halfwidth:end-thresh_halfwidth)>0;
    yl=get(gca,'ylim');
    plot(yl(1) +(spec_thresh-yl(1))*hloud,'g');
end

end
